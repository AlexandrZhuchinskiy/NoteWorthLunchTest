//
//  NLUIKitAssembly.swift
//  NLUIKit
//
//  Created by Alexandr Zhuchinskiy on 7/3/18.
//  Copyright © 2018 Alexandr Zhuchinskiy. All rights reserved.
//

import Foundation
import Swinject

public class NLUIKitAssembly: Assembly {

    public init() { }

    public func assemble(container: Container) {
        container.register(LoadingIndicatorProvider.self) { _ in
            return LoadingIndicator()
        }
    }

}
