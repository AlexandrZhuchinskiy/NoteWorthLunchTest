import Foundation
import Kingfisher

private struct Constant {
    static let imageUrlPathKey = "url"
    static let imageNameKey = "imageName"
    static let errorCodeKey = "KingfisherErrorCode"
    static let techEventName = "ImageError"
    static let widthParameterKey = "w"
}

public enum ImageSource: Equatable {
    case url(URL)
    case image(UIImage)
}

/**
 Provides an interface that a view should implement to use kingfisher library

 - Parameter URL: url for the image path
 - Parameter forceReload: retrieve the image from network instead of cached image
 */
public protocol ImageLoader {
    func loadSource(with source: ImageSource, forceReload: Bool, animated: Bool, onError: ((Error?) -> Void)?)
    func cancelLoadImage()
}

/**
 Provides image loading capability on UIImageView.
 */
extension ImageView: ImageLoader {
    /**
     The Kingfisher web url.
     */
    public var webURL: URL? {
        return self.kf.webURL
    }

    /**
     Loads images given a url. force reload is off by default.

     ## Usage Example: ##
     ````swift
     let url = URL(string: "http://example.com/nord.png"
     let imageView = UIImageView()
     imageView.loadImage(with: url)

     let url = URL(string: "http://example.com/earlyAccessBanner.png"
     let bannerImageView = UIImageView()
     bannerImageView.loadImage(with: url, forceReload: true)

     ````

     - Parameter URL: url for the image path
     - Parameter forceReload: retrieve the image from network instead of cached image
     - Parameter onError: optional callback

     */
    public func loadSource(with source: ImageSource, forceReload: Bool = false, animated: Bool = true, onError: ((Error?) -> Void)? = nil) {
        let resource: ImageResource

        switch source {
        case .image(let image):
            self.image = image
            return

        case .url(let url):
            resource = ImageResource(downloadURL: url)
        }

        let optionsInfo: KingfisherOptionsInfo
        if forceReload {
            optionsInfo = [.forceRefresh]
        } else {
            optionsInfo = []
        }

        if animated {
            alpha = 0.0
        }
        self.kf.setImage(with: resource, placeholder: nil, options: optionsInfo, progressBlock: nil, completionHandler: { image, error, _, url in
            if let error = error {
                if error.code == KingfisherError.badData.rawValue {
                    KingfisherManager.shared.cache.retrieveImage(forKey: resource.cacheKey, options: nil, completionHandler: { [weak self] (image, cacheType) in
                        guard let strongSelf = self else {
                            return
                        }

                        if cacheType == .none {
                            onError?(error)
                        } else {
                            strongSelf.image = image
                        }
                    })
                } else {
                    onError?(error)
                }
            }

            if animated {
                UIView.animate(withDuration: 0.3, animations: {
                    self.alpha = 1.0
                })
            }
        })
    }

    /**
     Cancels the loading of the current image.

     ## Usage Example: ##
     ````swift
     let url = URL(string: "http://example.com/nord.png")
     let imageView = UIImageView()
     imageView.loadImage(with: url)
     imageView.cancelLoadImage()
     ````
     */
    public func cancelLoadImage() {
        self.kf.cancelDownloadTask()
    }

}
