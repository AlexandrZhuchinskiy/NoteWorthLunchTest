//
//  RestaurantService.swift
//  NLApi
//
//  Created by Alexandr Zhuchinskiy on 6/28/18.
//  Copyright © 2018 Alexandr Zhuchinskiy. All rights reserved.
//

import Alamofire
import BrightFutures
import Foundation

public struct Location {
    let latitude: Double
    let longitude: Double
}

extension Location {
    public static func defaultLocation() -> Location {
        return Location(latitude: -33.8670522, longitude: 151.1957362)
    }

    func toString() -> String {
        return "\(latitude), \(longitude)"
    }
}

public class RestaurantService: BaseService {

    private enum Constants {
        static let restaurantURL = "/place/nearbysearch/json"
        static let typeKey = "type"
        static let keywordKey = "keyword"
        static let locationKey = "location"
        static let radiusKey = "radius"
        static let radius = 1500
    }

    public func listOfRestaurant(with keyword: String, location: Location = Location.defaultLocation(), type: String) -> Future<RestaurantSearchResult, APIError> {
        let params: Parameters = [
            Constants.typeKey: type,
            Constants.keywordKey: keyword,
            Constants.locationKey: location.toString(),
            Constants.radiusKey: Constants.radius
        ]
        return performRequest(with: Constants.restaurantURL, params: params)
    }

}
