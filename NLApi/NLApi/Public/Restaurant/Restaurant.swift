//
//  Restaurant.swift
//  NLApi
//
//  Created by Alexandr Zhuchinskiy on 6/28/18.
//  Copyright © 2018 Alexandr Zhuchinskiy. All rights reserved.
//

import Foundation

public struct RestaurantInfo: Codable {
    public let name: String
    public let icon: String
    public let rating: Double

    enum CodingKeys: String, CodingKey {
        case name = "name"
        case icon = "icon"
        case rating = "rating"
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try container.decode(String.self, forKey: CodingKeys.name)
        self.icon = try container.decode(String.self, forKey: CodingKeys.icon)
        self.rating = try container.decode(Double.self, forKey: CodingKeys.rating)
    }
}

public struct RestaurantSearchResult: Codable {

    public let restaurants: [RestaurantInfo]

    enum CodingKeys: String, CodingKey {
        case restaurants = "results"
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.restaurants = try container.decode([RestaurantInfo].self, forKey: .restaurants)
    }
}

extension RestaurantSearchResult: NLDecodable {
    public static func decode(data: Data) throws -> RestaurantSearchResult {
        let decoder = JSONDecoder()
        let restaurant = try decoder.decode(RestaurantSearchResult.self, from: data)
        return restaurant
    }
}

extension RestaurantInfo: NLDecodable {
    public static func decode(data: Data) throws -> RestaurantInfo {
        let decoder = JSONDecoder()
        let restaurant = try decoder.decode(RestaurantInfo.self, from: data)
        return restaurant
    }
}
