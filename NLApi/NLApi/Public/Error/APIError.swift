//
//  APIError.swift
//  NLApi
//
//  Created by Alexandr Zhuchinskiy on 6/28/18.
//  Copyright © 2018 Alexandr Zhuchinskiy. All rights reserved.
//

import Foundation

public enum APIError: Error {
    case userCanceled
    case jsonParsing
    case otherError(error: Error)
}

extension Error {
    func tryAsAPIError() -> APIError {
        return APIError.otherError(error: self)
    }
}

