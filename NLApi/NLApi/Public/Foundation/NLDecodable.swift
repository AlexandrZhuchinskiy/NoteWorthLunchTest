//
//  NLDecodable.swift
//  NLApi
//
//  Created by Alexandr Zhuchinskiy on 6/28/18.
//  Copyright © 2018 Alexandr Zhuchinskiy. All rights reserved.
//

import Foundation

public protocol NLDecodable {
    static func decode(data: Data) throws -> Self
}

public func decode<T: NLDecodable>(_ data: Data) throws -> T {
    return try T.decode(data: data)
}
