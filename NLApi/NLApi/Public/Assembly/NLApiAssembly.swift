//
//  NLApiAssembly.swift
//  NLApi
//
//  Created by Alexandr Zhuchinskiy on 6/28/18.
//  Copyright © 2018 Alexandr Zhuchinskiy. All rights reserved.
//

import Swinject

public class NLApiAssembly: Assembly {

    public init() { }

    public func assemble(container: Container) {
        container.register(RestaurantService.self) { _ in
            return RestaurantService()
        }
    }

}
