//
//  BaseService.swift
//  NLApi
//
//  Created by Alexandr Zhuchinskiy on 6/28/18.
//  Copyright © 2018 Alexandr Zhuchinskiy. All rights reserved.
//

import Alamofire
import BrightFutures

public class BaseService {

    private enum Constants {
        static let baseURL = "https://maps.googleapis.com/maps/api"
        static let key = "key"
        static let apiKey = "AIzaSyC5_uh3_p3AngpOT48LCZEhmncNPo1K7lU"
    }

    func performRequest<T: NLDecodable>(with path: String, params: Parameters? = nil, method: HTTPMethod = .get) -> Future<T, APIError> {
        return Future { completion in
            let url = Constants.baseURL + path
            var parametrs = params
            parametrs?[Constants.key] = Constants.apiKey
            Alamofire.request(url, method: method, parameters: parametrs, encoding: URLEncoding.queryString)
                .validate()
                .responseData { (response) in
                    switch response.result {
                    case .success(let data):
                        do {
                            let model = try T.decode(data: data)
                            completion(.success(model))
                        }
                        catch {
                            completion(.failure(.jsonParsing))
                        }
                    case .failure(let error):
                        completion(.failure(error.tryAsAPIError()))
                    }
            }
        }
    }
}
