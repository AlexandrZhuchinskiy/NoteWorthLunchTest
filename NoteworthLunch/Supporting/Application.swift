//
//  Application.swift
//  NoteworthLunch
//
//  Created by Alexandr Zhuchinskiy on 6/28/18.
//  Copyright © 2018 Alexandr Zhuchinskiy. All rights reserved.
//

import Foundation

import Foundation
import Swinject
import SwinjectStoryboard
import NLUIKit

import NLApi

private protocol NLUIApplicationRoot {
    var strongDelegate: UIApplicationDelegate? { get set }
    func createAssembler() -> Assembler
    func createAssembler(container: Container) -> Assembler
}

extension NLUIApplicationRoot {
    func createAssembler() -> Assembler {
        return createAssembler(container: SwinjectStoryboard.defaultContainer)
    }

    func createAssembler(container: Container) -> Assembler {
        let assemblies: [Assembly] = [
            NLApiAssembly(),
            NLUIKitAssembly(),
            NLMainAssembly()
        ]
        return Assembler(assemblies, container: container)
    }
}

/**
 Provides an entry point for our application. We use this class as the composition root for the purposes of dependency injection.

 - note: We need to hold onto a copy of the application delegate we create due to the aggressive nature of ARC and our superclass delegate property being weak.
 */
class Application: UIApplication, NLUIApplicationRoot {

    fileprivate var strongDelegate: UIApplicationDelegate?
    private var assembler: Assembler!

    override init() {
        super.init()

        assembler = createAssembler()

        Container.loggingFunction = nil

        let resolver = assembler.resolver
        let appDelegate = resolver.forceResolve(AppDelegate.self)

        strongDelegate = appDelegate
        delegate = strongDelegate
    }
}
