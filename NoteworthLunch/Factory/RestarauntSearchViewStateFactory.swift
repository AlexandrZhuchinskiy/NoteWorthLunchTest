import Foundation
import NLApi

struct RestarauntSearchViewState {
    struct Restaraunt {
        let name: String
        let rating: Double
        let icon: URL?
    }

    let restaraunts: [Restaraunt]
}

class RestarauntSearchViewStateFactory {
    func make(from result: RestaurantSearchResult) -> RestarauntSearchViewState {
        let restaraunts = result.restaurants.map { RestarauntSearchViewState.Restaraunt(name: $0.name, rating: $0.rating, icon: URL(string: $0.icon)) }
        return RestarauntSearchViewState(restaraunts: restaraunts)
    }
}
