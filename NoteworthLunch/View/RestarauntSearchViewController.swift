import NLUIKit
import NLFoundation

enum RestarauntSearchViewEvent {
    case viewDidLoad
    case search(with: String)
}

class RestarauntSearchViewController: UIViewController {

    private enum Constants {
        static let cellID = "RestarauntSearchResultCellID"
        static let estimatedCellHeight: CGFloat = 80.0
    }

    @IBOutlet private var tableView: UITableView! {
        didSet {
            tableView.rowHeight = UITableViewAutomaticDimension
            tableView.estimatedRowHeight = Constants.estimatedCellHeight
        }
    }

    @IBOutlet private var searchBar: UISearchBar!

    private var viewState: RestarauntSearchViewState? {
        didSet {
            tableView.reloadData()
        }
    }

    private var kitchen: AnyKitchen<RestarauntSearchViewEvent, RestarauntSearchCommand>!
    private var loadingIndicator: LoadingIndicatorProvider!

    func inject(kitchen: AnyKitchen<RestarauntSearchViewEvent, RestarauntSearchCommand>, loadingIndicator: LoadingIndicatorProvider) {
        self.kitchen = kitchen
        self.loadingIndicator = loadingIndicator
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        kitchen.receive(event: .viewDidLoad)
    }

    private func showErrorBanner(with error: Error) {
        // TODO: - Implement show banner
    }
}

extension RestarauntSearchViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        kitchen.receive(event: .search(with: searchBar.text ?? ""))
        searchBar.resignFirstResponder()
    }
}

extension RestarauntSearchViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewState?.restaraunts.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.cellID, for: indexPath) as! RestarauntSearchResultCell
        cell.configure(with: forceCast(viewState!.restaraunts[indexPath.row], RestarauntSearchViewState.Restaraunt.self, justification: .cantBeNilAtThisPoint))
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension RestarauntSearchViewController: KitchenDelegate {
    func perform(_ command: RestarauntSearchCommand) {
        switch command {
        case .title(let title):
            self.title = title
        case .startLoading:
            loadingIndicator.show(in: view)
        case .finishedLoading:
            loadingIndicator.hide()
        case .present(let viewState):
            self.viewState = viewState
        case .error(let error):
            showErrorBanner(with: error)
        }
    }
}
