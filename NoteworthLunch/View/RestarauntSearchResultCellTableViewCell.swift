//
//  RestarauntSearchResultCellTableViewCell.swift
//  NoteworthLunch
//
//  Created by Alexandr Zhuchinskiy on 6/28/18.
//  Copyright © 2018 Alexandr Zhuchinskiy. All rights reserved.
//

import NLUIKit

class RestarauntSearchResultCell: UITableViewCell {

    @IBOutlet private var nameLabel: UILabel!
    @IBOutlet private var ratingLabel: UILabel!
    @IBOutlet private var iconImageView: UIImageView!

    func configure(with viewState: RestarauntSearchViewState.Restaraunt) {
        nameLabel.text = viewState.name
        ratingLabel.text = String(viewState.rating)
        if let iconURL = viewState.icon {
            loadImage(url: iconURL)
        }
    }

    private func loadImage(url: URL) {
        iconImageView.loadSource(with: .url(url))
    }

    override func prepareForReuse() {
        ratingLabel.text = nil
        nameLabel.text = nil
        iconImageView.image = nil
    }
}

