//
//  NLMainAssembly.swift
//  NoteworthLunch
//
//  Created by Alexandr Zhuchinskiy on 6/28/18.
//  Copyright © 2018 Alexandr Zhuchinskiy. All rights reserved.
//

import Foundation
import Swinject
import SwinjectStoryboard
import NLApi
import NLFoundation
import NLUIKit

class NLMainAssembly: Assembly {

    func assemble(container: Container) {

        container.register(AppDelegate.self) { _ in
            return AppDelegate()
        }.inObjectScope(.container)

        container.register(RestarauntSearchViewStateFactory.self) { _ in
            return RestarauntSearchViewStateFactory()
        }

        container.register(RestarauntSearchKitchen.self) { resolver in
            let restaurantService = resolver.forceResolve(RestaurantService.self)
            let viewStateFactory = resolver.forceResolve(RestarauntSearchViewStateFactory.self)
            return RestarauntSearchKitchen(restarauntService: restaurantService, viewStateFactory: viewStateFactory)
        }

        container.storyboardInitCompleted(RestarauntSearchViewController.self) { resolver, controller in
            let kitchen = AnyKitchen(resolver.forceResolve(RestarauntSearchKitchen.self))
            controller.inject(kitchen: kitchen, loadingIndicator: resolver.forceResolve(LoadingIndicatorProvider.self))

            kitchen.delegate = AnyKitchenDelegate(controller)
        }
    }

}
